@extends('layouts.app')

@section('content')
    <section class="bg-grey">
        <div class="container mt-3">
            <div class="p-3" style="background-color: #FFFFFF; border:1px solid rgba(0, 0, 0, 0.125); border-bottom: none; border-radius: 2px;">
                <a href="/market-insights" class="btn btn-outline-secondary"><i class="fas fa-long-arrow-alt-left"></i></a>
                @auth
                    @if(Auth::user()->id == $post->user_id)
                        <a href="/posts/{{$post->id}}/edit" class="btn btn-outline-primary float-right ml-2"><i class="fas fa-edit"></i></a>

                        {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-right'])!!}
                            {{Form::hidden('_method', 'DELETE')}}
                            {{Form::submit('&times;', ['class' => 'btn btn-outline-danger font-weight-bold'])}}

                        {!!Form::close()!!}
                    @endif
                @endauth
            </div>

            <div class="card p-3 mb-3 post-single" style="border-top: none;">
                <h1>{{$post->title}}</h1>
                <h2 class="mt-1 text-capitalize"><a href="/post/categories/{{$post->topic['slug']}}">{{$post->topic['title']}}</a></h2>
                <hr>
                <img style="width: 100%;" class="animated fadeIn" src="/storage/cover_images/{{$post->cover_image}}">
                <p>{!!$post->body!!}</p>
                <hr>
                <small class="float-right" style="font-style: italic;">Published - <strong>{{$post->created_at->format('D d M Y')}}</strong> by <strong>{{$post->topic['title']}} Market Insights Team</strong></small>
            </div>
        </div>
    </section>
@endsection