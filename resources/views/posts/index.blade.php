@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row my-3 aos-item" data-aos="zoom-in">
        <div class="col-12 blue-heading pt-2">
            <h2>Market Insights</h2>
        </div>
    </div>
</div>

<section class="bg-grey">
    <div class="container">
        <article class="row animated fadeIn fast">
        @if(count($posts) > 0)
            @foreach($posts as $post)

                        <div class="col-sm-6">
                            <div class="card blog-item mt-3">
                                <img src="/storage/cover_images/{{$post->cover_image}}" class="img-fluid rounded  animated fadeIn fast" alt="A generic square placeholder image with rounded corners in a figure.">
                                <h3><a href="market-insights/{{$post->slug}}">{{$post->title}}</a></h3>
                                <hr>
                                <h4>{{$post->topic['title']}}</h4>
                                <p><i class="fas fa-calendar-alt"></i> {{$post->created_at->format('d/m/Y')}}</p>
                                <a href="market-insights/{{$post->slug}}"><button class="blog-link">Read Article</button></a>
                            </div>
                        </div>


            @endforeach
        </article>
            {{$posts->links()}}
        @else
            <p>No Posts Found</p>
        @endif

    </div>
</section>
@endsection
