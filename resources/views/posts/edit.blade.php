@extends('layouts.app')

@section('content')
    <div class="container my-5">
        <h3 class="pb-4">Edit Post</h3>
        {!! Form::open(['action' => ['PostsController@update', $post->id], 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            {{Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('topic', 'Topic')}}
            {{Form::select('topic', $topics, $post->topic_id, ['class' => 'form-control', 'placeholder' => 'Topic'])}}
        </div>
        <div class="form-group">
            {{Form::label('created_at', 'Created At')}}
            {{Form::text('created_at', $post->created_at, ['class' => 'form-control', 'placeholder' => 'Created At'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', $post->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
        </div>
        <div class="form-group">
            {{Form::file('cover_image')}}
        </div>

        {{ method_field('PATCH')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary mt-3'])}}
        {!! Form::close() !!}
    </div>
@endsection

@push('scripts')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
@endpush
