@extends('layouts.app')

@section('content')
    <div class="container my-5">
        <h3 class="pb-4">Edit Category</h3>
        {!! Form::open(['action' => ['TopicsController@update', $topic->id], 'POST']) !!}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            {{Form::text('title', $topic->title, ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        {{ method_field('PATCH')}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary mt-3'])}}
        {!! Form::close() !!}
    </div>
@endsection