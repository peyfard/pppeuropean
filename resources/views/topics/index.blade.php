@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row my-3 aos-item" data-aos="zoom-in">
            <div class="col-12 blue-heading pt-2">
                <h2>News Categories</h2>
            </div>
        </div>
    </div>
    <section class="bg-grey">
        <section class="container mt-1 mb-5">
            <article>
                <div class="row">
                    @if(count($topics) > 0)
                        @foreach($topics as $topic)

                                    <div class="col-md-12">
                                        <div class="card animated fadeInUp fast mb-4 p-4">
                                            <h3><a href="/post/categories/{{$topic->slug}}">{{$topic->title}}</a></h3>
                                        </div>
                                    </div>
                        @endforeach
                    @else
                        <p>No Topics Found</p>
                    @endif
                </div>
            </article>
        </section>
    </section>
@endsection
