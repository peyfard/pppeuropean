@extends('layouts.app')

@section('content')
    <div class="container my-5">
        <h3 class="pb-4">Create Category</h3>
        {!! Form::open(['action' => 'TopicsController@store', 'POST']) !!}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        {{Form::submit('Submit', ['class' => 'btn btn-primary mt-3'])}}
        {!! Form::close() !!}
    </div>
@endsection
