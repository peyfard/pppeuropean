
<nav class="navbar navbar-expand-md navbar-dark navbar-laravel">
            <a class="branding mr-4" href="{{ url('/') }}">
                <img src="{{ URL::to('/') }}/css/assets/logo_ppp.png" class="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">


                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown border-right">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Sectors <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu green-dropdown" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item green-item" href="/sectors/automotive">Automotive</a>
                            <a class="dropdown-item green-item" href="/sectors/components">Components</a>
                            <a class="dropdown-item green-item" href="/sectors/petrochemical">Petrochemical</a>
                            <a class="dropdown-item green-item" href="/sectors/power-generation">Power Generation</a>
                            <a class="dropdown-item green-item" href="/sectors/property-development">Property Development</a>
                            <a class="dropdown-item green-item" href="/sectors/health-and-supplements">Health &amp; Supplements</a>
                            <a class="dropdown-item green-item" href="/sectors/skincare">Skincare</a>
                        </div>
                    </li>
                    <li class="nav-item border-right px-2">
                        <a class="nav-link" href="/our-ethos">Our Ethos</a>
                    </li>
                    <li class="nav-item border-right px-2">
                        <a class="nav-link" href="/market-insights">Market Insights</a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link" href="/contact-us">Contact Us</a>
                    </li>
                    <!-- Authentication Links -->
                    @guest

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(Auth::user()->access_level == 1)
                                        <a class="dropdown-item" href="/users/">Manage Users</a>
                                        <a class="dropdown-item" href="/dashboard/messages">Messages</a>
                                    @endif
                                    @if(Auth::user()->access_level !== 1)
                                        <a class="dropdown-item" href="/users/{{ Auth::user()->id }}">Manage Profile</a>
                                    @endif
                                <a class="dropdown-item" href="/dashboard">
                                    Dashboard
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
</nav>