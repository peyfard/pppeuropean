<footer>
    <div class="container">
        <div class="row">
            <div class="col-5">
                <p>&copy; PPP European 2024</p>
            </div>
            <div class="col-7">
                <p class="float-right"><a href="/terms-and-conditions">Terms and Conditions </a> | <a href="/cookie-policy"> Cookies</a></p>
            </div>
        </div>
    </div>
</footer>