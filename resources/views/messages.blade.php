@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row my-3 aos-item" data-aos="zoom-in">
            <div class="col-12 blue-heading pt-2">
                <h2>Messages</h2>
            </div>
        </div>
    </div>

    <section class="bg-grey">
        <div class="container mt-1 mb-5">
            @if(count($messages) > 0)
                @foreach($messages as $message)
                    <article class="card animated fadeIn fast mb-4 p-4">
                        <div class="row">
                            <div class="col-md-4">
                                <h3>{{$message->name}}</h3>
                                <p>{{$message->email}}</p>
                                <p>{{$message->company}}</p>
                                <p>{{$message->reason}}</p>
                            </div>
                            <div class="col-md-8">
                                <p>{{$message->body}}</p>
                            </div>
                        </div>
                    </article>
                @endforeach
                {{$messages->links()}}
            @else
                <p>No Messages Found</p>
            @endif
        </div>
    </section>
@endsection
