@extends('layouts.app')
@section('content')

    <section class="bg-white">
        <div class="container">
            <div class="row my-3">
                <div class="homepage-carousel">
                    <div class="col-sm-12 col-md-6">
                        <div class="mt-3">
                            <h2>Corporate Responsibility</h2>
                            <p class="pt-3">We are an independent group operating across a variety of business
                                sectors worldwide. Our business ethos is to strive to be fully customer
                                focused by offering value through innovation and service whilst maintaining
                                our commitment to environment integrity. Our business partners can be confident
                                in the knowledge that we always strive to work openly and fairly.
                                We understand the importance of working collaboratively. PPP Group places
                                great importance on effective and open communication.</p>
                            <a class="mt-5" href="/our-ethos">Find out more <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-grey">
        <div class="container">
            <div class="row py-3">
                <div class="col-sm-4">
                    <div class="card blog-item">
                        <img src="/css/assets/automotive.jpg" class="img-fluid rounded animated fadeIn fast" alt="A generic square placeholder image with rounded corners in a figure.">
                        <a href="/sectors/automotive"><h3>Automotive innovation for the future</h3></a>
                        <hr>
                        <p>The future of automotive is what we strive to acheive. See
                            how our technological advancements are driving the global
                            market to the future.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card blog-item">
                        <img src="/css/assets/power-gen.jpg" class="img-fluid rounded  animated fadeIn fast" alt="A generic square placeholder image with rounded corners in a figure.">
                        <a href="/sectors/power-generation"><h3>Renewable Energy</h3></a>
                        <hr>
                        <p>Our specialised team can provide expert guidance to power generation firms, helping them navigate regulatory complexities and optimise operations.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card blog-item">
                        <img src="/css/assets/petrochemical.jpg" class="img-fluid rounded  animated fadeIn fast" alt="A generic square placeholder image with rounded corners in a figure.">
                        <a href="/sectors/petrochemical"><h3>Petrochemical Solutions</h3></a>
                        <hr>
                        <p>We work in close collaboration with petrochemical companies in the Middle East to satisfy our customers' requirements.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sector-white">
        <div class="container">
            <div class="row mt-5 aos-item" data-aos="zoom-in">
                <div class="col-12 blue-heading">
                    <h2>Global Sectors</h2>
                </div>
            </div>
            <div class="row sectors aos-item" data-aos="zoom-in">
                <div class="col-sm-3">
                    <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Automotive</a>
                        <a class="list-group-item list-group-item-action" id="list-components-list" data-toggle="list" href="#list-components" role="tab" aria-controls="components">Components</a>
                        <a class="list-group-item list-group-item-action" id="list-petrochemical-list" data-toggle="list" href="#list-petrochemical" role="tab" aria-controls="petrochemical">Petrochemical</a>
                        <a class="list-group-item list-group-item-action" id="list-power-list" data-toggle="list" href="#list-power" role="tab" aria-controls="power">Power Generation</a>
                        <a class="list-group-item list-group-item-action" id="list-property-list" data-toggle="list" href="#list-property" role="tab" aria-controls="property">Property Development</a>
                        <a class="list-group-item list-group-item-action" id="list-health-list" data-toggle="list" href="#list-health" role="tab" aria-controls="health">Health &amp; Supplements</a>
                        <a class="list-group-item list-group-item-action" id="list-skincare-list" data-toggle="list" href="#list-skincare" role="tab" aria-controls="skincare">Skincare</a>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active card sector-information p-3" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                            We fully appreciate the importance of driving dynamics, the pleasure and performance expectations of today’s
                            driver, which is always a key consideration. The effortless deployment of power makes a significant difference to
                            the safety and comfort of vehicles improving driver performance.<br><br>

                            Electric vechicles are far more than just an environmental choice in today’s market. Through improved electric
                            charger converter technology, such as the integrated charger-inverter, affordable and efficient electric cars are
                            now being produced on a large scale.
                            <br><br>
                            <a class="mr-3" href="/sectors/automotive">Find our more <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                        <div class="tab-pane fade card sector-information p-3" id="list-components" role="tabpanel" aria-labelledby="list-components-list">
                            The European automotive industry is among the largest and most innovative in the world, with a strong focus on quality, safety,
                            and sustainability. As this industry transitions towards electric vehicles (EVs), stricter emission standards, and advanced
                            driver-assistance systems (ADAS), the need for expert consultancy in automotive component manufacturing is growing.<br><br>
                            <a class="mr-3" href="/sectors/components">Find our more <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                        <div class="tab-pane fade card sector-information p-3" id="list-petrochemical" role="tabpanel" aria-labelledby="list-petrochemical-list">
                            At PPP European Group our team provides expert services to companies in the petrochemical sector, focusing on improving operational efficiency, regulatory compliance, digital transformation, and sustainability. With the rising focus on decarbonisation, circular economies, and green technologies, our firm will be a strategic partner in guiding petrochemical businesses through these transformations, ensuring they remain profitable while meeting future environmental and regulatory requirements. <br><br>
                            <a class="mr-3" href="/sectors/petrochemical">Find out more <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                        <div class="tab-pane fade card sector-information p-3" id="list-power" role="tabpanel" aria-labelledby="list-power-list">
                            At PPP European Group our specialised team can provide expert guidance to power generation firms, helping them navigate regulatory complexities, optimise operations, adopt new technologies, and integrate sustainable practices.<br><br>
                            <a class="mr-3" href="/sectors/power-generation">Find out more <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                        <div class="tab-pane fade card sector-information p-3" id="list-property" role="tabpanel" aria-labelledby="list-property-list">
                            At PPP European Group we focus on acquiring, developing, and selling commercial and residential real estate, ensuring each project maximises returns while meeting market needs for eco-friendly and technologically advanced properties.<br><br>
                            <a class="mr-3" href="/sectors/property-development">Find out more <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                        <div class="tab-pane fade card sector-information p-3" id="list-health" role="tabpanel" aria-labelledby="list-health-list">
                            As consumers demand more personalised and natural health solutions, companies in the supplements industry face the challenge of meeting these evolving trends while maintaining regulatory compliance and ensuring product quality. At PPP European group our team provides strategic guidance to companies in the vitamin and dietary supplements space, helping them navigate the complex regulatory landscape, optimise their product formulations, and implement successful marketing strategies.<br><br>
                            <a class="mr-3" href="/sectors/health-and-supplements">Find out more <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                        <div class="tab-pane fade card sector-information p-3" id="list-skincare" role="tabpanel" aria-labelledby="list-skincare-list">
                            At PPP European Group, having been involved with the Middle-Eastern markets for over three decades, our industry experts understand the specific needs of the market and the cultural mystique that surrounds it, based on that we have designed our Aesthetica® range to address these needs.<br><br>
                            <a class="mr-3" href="/sectors/skincare">Find out more <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection