@extends('layouts.app')
@section('content')
    <section class="sectors-carousel power-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Power Generation</h1>
                    <p>At PPP European Group our specialised team can provide expert guidance to power generation firms, helping them navigate regulatory complexities, optimise operations, adopt new technologies, and integrate sustainable practices.
                    </p>
                    <hr>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content">
                    <h2>Sustainable Energy</h2>
                    <hr>
                    <p>
                        The global power generation industry is undergoing a significant transformation,
                        driven by the shift towards renewable energy, decarbonisation efforts, digitalisation,
                        and the need for greater efficiency. As governments push for carbon neutrality and
                        cleaner energy production, power generation companies face challenges in
                        transitioning from traditional fossil fuel-based plants to renewable energy sources
                        like wind, solar, and hydropower. At PPP European Group our specialised team can
                        provide expert guidance to power generation firms, helping them navigate regulatory
                        complexities, optimise operations, adopt new technologies, and integrate sustainable
                        practices.
                    </p>
                </div>
                <div class="col-md-4 pt-4">
                    <img src="/css/assets/power-petro/energy.png" class="img-fluid my-5" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content order-md-12">
                    <h2>Our Team</h2>
                    <hr>
                    <p>Our team will assist power generation companies in optimising their operations,
                        integrating renewable energy solutions, complying with global energy regulations,
                        and leveraging digital technologies to improve efficiency. With a specific focus on
                        renewable energy transition, decarbonisation strategies, and emerging technologies
                        such as energy storage, our firm will serve as a strategic partner in driving
                        sustainable growth for power generation businesses.

                    </p>

                </div>
                <div class="col-md-4 order-md-1 mt-2 pt-2">
                    <img src="/css/assets/power-petro/power.png" class="img-fluid pt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
@endsection