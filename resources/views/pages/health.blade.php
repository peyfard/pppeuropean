@extends('layouts.app')
@section('content')
    <section class="sectors-carousel health-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Vitamins &amp; Dietary Supplements</h1>
                    <hr>
                    <p>To assist individuals meet their personal nutritional requirements we have developed
                        an extensive range of vitamin and mineral supplements.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-7 sector-content">
                    <h2>Market Analysis</h2>
                    <hr>
                    <p>The global vitamin and dietary supplements market has seen significant growth in
                        recent years, driven by increasing health awareness, aging populations, and a
                        growing interest in preventative healthcare. The global dietary supplements market
                        was valued at over $150 billion in 2023, with projected growth of over 8% annually.
                        The increasing demand for immune-boosting products, personalised nutrition, and
                        plant-based supplements is driving expansion, along with a rising consumer focus on
                        preventive health and wellness.<br><br>As consumers demand more personalised and
                        natural health solutions, companies in the supplements industry face the challenge
                        of meeting these evolving trends while maintaining regulatory compliance and
                        ensuring product quality. At PPP European group our team provides strategic
                        guidance to companies in the vitamin and dietary supplements space, helping them
                        navigate the complex regulatory landscape, optimise their product formulations, and
                        implement successful marketing strategies.</p>
                </div>
                <div class="col-md-5 pt-4">
                    <img src="/css/assets/health-property/lifestyle.png" class="img-fluid my-3" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content order-md-12">
                    <h2>Our Team</h2>
                    <hr>
                    <p>Our team offers expert advice to vitamin and dietary supplement companies,
                        focusing on product development, regulatory compliance, and marketing strategies.
                        As the supplements industry continue to grow and diversify, we aim to help
                        businesses differentiate their products, navigate complex regulations, and align with
                        the latest consumer health trends. Whether clients are launching new products,
                        optimising existing formulations, or expanding into new markets, our services will
                        ensure they meet consumer demands while adhering to industry regulations.

                    </p>

                </div>
                <div class="col-md-4 order-md-1 mt-2">
                    <img src="/css/assets/health-property/supplements.png" class="img-fluid pt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
@endsection