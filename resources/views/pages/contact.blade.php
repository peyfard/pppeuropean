@extends('layouts.app')
@section('content')
    <section class="contact-carousel">
        <div class="container my-1">
            <div class="row">
                <div class="col-md-12 sector-content">
                    <h2 class="pb-4">Contact Us</h2>
                    <p>From business enquiries to more information about what we do, we’re happy to assist. For urgent contact please either email us at <a href="mailto:sales@pppeuropeangroup.com">sales@pppeuropeangroup.com</a></p>
                    <hr>
                </div>
                <div class="col-md-6">
                    {!! Form::open(['action' => 'MessagesController@store', 'POST']) !!}
                        <div class="form-group">
                            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group">
                            {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'Email'])}}
                        </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group">
                            {{Form::text('company', '', ['class' => 'form-control', 'placeholder' => 'Company Name'])}}
                        </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group">
                            {{Form::text('reason', '', ['class' => 'form-control', 'placeholder' => 'Reason for contact'])}}
                        </div>
                </div>
                <div class="col-md-12">
                        <div class="form-group">
                            {{Form::textarea('body', '', ['class' => 'form-control', 'placeholder' => 'Message'])}}
                        </div>
                    <div class="g-recaptcha" data-sitekey="6Lfjl8EUAAAAADiW2Lx_5cYWvanvv0gjHQ7NaQF6"></div>

                    {{Form::submit('Submit', ['class' => 'btn btn-primary mt-3'])}}
                </div>
                        {!! Form::close() !!}
                </div>
        </div>
    </section>

    <section class="bg-grey">
        <div class="container">
            <div class="row py-3">
                <div class="col-sm-4">
                    <div class="card blog-item">
                        <img src="/css/assets/automotive.jpg" class="img-fluid rounded animated fadeIn fast" alt="A generic square placeholder image with rounded corners in a figure.">
                        <a href="/sectors/automotive"><h3>Automotive innovation for the future</h3></a>
                        <hr>
                        <p>The future of automotive is what we strive to acheive. See
                            how our technological advancements are driving the global
                            market to the future.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card blog-item">
                        <img src="/css/assets/power-gen.jpg" class="img-fluid rounded  animated fadeIn fast" alt="A generic square placeholder image with rounded corners in a figure.">
                        <a href="/sectors/power-generation"><h3>Renewable Energy</h3></a>
                        <hr>
                        <p>Our specialised team can provide expert guidance to power generation firms, helping them navigate regulatory complexities and optimise operations.</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card blog-item">
                        <img src="/css/assets/petrochemical.jpg" class="img-fluid rounded  animated fadeIn fast" alt="A generic square placeholder image with rounded corners in a figure.">
                        <a href="/sectors/petrochemical"><h3>Petrochemical Solutions</h3></a>
                        <hr>
                        <p>We work in close collaboration with petrochemical companies in the Middle East to satisfy our customers' requirements.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container my-1">
            <div class="row my-3">
                <div class="col-md-4 sales-info pb-3">
                    <h3>Find Us</h3>
                    <hr>
                    <h5><i class="far fa-building"></i>St. James Business Park</h5>
                    <h5><i class="fas fa-road"></i>Henwood Road</h5>
                    <h5><i class="fas fa-city"></i>Ashford</h5>
                    <h5><i class="fas fa-globe"></i>England</h5>
                    <h5><i class="far fa-address-card"></i>TN22 6BH</h5>
                </div>
                <div class="col-md-8">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2502.69761671504!2d0.8825954519136437!3d51.150927044423774!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47deda5ed0061707%3A0x3e3fdfbbc9e232c6!2sP+P+P+(UK)+Ltd!5e0!3m2!1sen!2suk!4v1544745982822" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>

@endsection