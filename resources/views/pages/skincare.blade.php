@extends('layouts.app')
@section('content')
    <section class="sectors-carousel skincare-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Skincare</h1>
                    <hr>
                    <p>Aesthetica&#174; is a new range of targeted skincare products specifically for the Middle-Eastern market.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-7 sector-content">
                    <h2>Aesthetica&#174;</h2>
                    <hr>
                    <p>Combining the latest skincare science and leading UK dermatological expertise, our team has designed a new range of health and beauty products specifically for the Middle Eastern market. Launching in the spring of 2025, the range includes:</p>
                    <h6>
                    Day Creams<br>
                        Rejuvenating Night Creams<br>
                        Firming &amp; Anti-Aging Serums<br>
                        Nourishing Cleansing Cream<br>
                        Purifying Cleansing Gel<br>
                        Eye Creams<br>
                        Exfoliating Cleansers<br>
                        Ultrasonic Fascial Massage Units
                    </h6>

                </div>
                <div class="col-md-5 pt-4">
                    <img src="/css/assets/health-property/aesthetica.png" class="img-fluid my-3" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content order-md-12">
                    <h2>Market Research</h2>
                    <hr>
                    <p>At PPP European Group, having been involved with the Middle-Eastern markets for over three decades, our industry experts understand the specific needs of the market and the cultural mystique that surrounds it, based on that we have designed our Aesthetica&#174; range to address these needs.  With our products, we strive for excellence in everything that we do.  From our finest clinically proven ingredients through to the beautifully designed bespoke packaging for our skincare range.

                    </p>

                </div>
                <div class="col-md-4 order-md-1 mt-2">
                    <img src="/css/assets/health-property/market-research.png" class="img-fluid pt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
@endsection