@extends('layouts.app')
@section('content')
    <section class="sectors-carousel petrochemical-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Petrochemical</h1>
                    <hr>
                    <p>At PPP European Group our team provides expert services to companies in the petrochemical sector, focusing on improving operational efficiency, regulatory compliance, digital transformation, and sustainability.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content">
                    <h2>Overview</h2>
                    <hr>
                    <p>The global petrochemical industry plays a critical role in producing essential
                        materials for various industries, including plastics, chemicals, fertilizers, and
                        synthetic fibres. With increasing environmental regulations, the shift toward
                        sustainability, and fluctuating oil prices, the industry faces significant challenges and
                        opportunities. At PPP European Group our team provides expert services to
                        companies in the petrochemical sector, focusing on improving operational efficiency,
                        regulatory compliance, digital transformation, and sustainability. With the rising focus
                        on decarbonisation, circular economies, and green technologies, our firm will be a
                        strategic partner in guiding petrochemical businesses through these transformations,
                        ensuring they remain profitable while meeting future environmental and regulatory
                        requirements. Our aim is to provide value-driven solutions to our clients, ensuring
                        long-term success in a dynamic and challenging market.</p>
                </div>
                <div class="col-md-4 pt-5">
                    <img src="/css/assets/power-petro/petrochemical_distillation.png" class="img-fluid my-3" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>

@endsection