@extends('layouts.app')
@section('content')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">{{$title}}</h1>
            <hr>
            <p class="lead">This is a a simple page to list some services that the company does.</p>
        </div>
    </div>
    <div class="container my-3">
        @extends('inc.messages')
        @if(count($services) > 0)
            <ul class="list-group">
                @foreach($services as $service)
                    <li class="list-group-item mb-2">{{$service}}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endsection
