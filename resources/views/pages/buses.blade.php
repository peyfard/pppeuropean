@extends('layouts.app')
@section('content')
    <section class="sectors-carousel buses-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Buses &amp; Minibuses</h1>
                    <hr>
                    <p>APF Coaches, a subsidiary of PPP European
                        Group in the Middle East, will be showcasing a new range of buses and mini-buses.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content">
                    <h2>Buses</h2>
                    <hr>
                    <p>Our range includes state of the art environmentally friendly hybrid-diesel-electric buses
                        to meet our rapidly expanding cities’ growing demand. Our buses provide increased passenger
                        capacity and comfort. Smoother performance is achieved through our use of electric powertrains,
                        which provide precisely controlled smoother acceleration leading to an enhanced passenger
                        experience and driver control.<br><br>Our new range of buses provide up to 30% fuel savings, which
                        is achieved through lighter body design, more energy efficient engines and more effective use
                        of electric motors in varying traffic conditions and stop start situations.</p>
                </div>
                <div class="col-md-4 pt-4">
                    <img src="/css/assets/automotive/cvt_performance.png" class="img-fluid my-3" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-7 sector-content order-md-12">
                    <h2>Hybrid Technology</h2>
                    <hr>
                    <p>Through the use of hybrid technology, our new buses will achieve up to 50% lower exhaust emissions.
                        This is achieved by being emission-free at bus stops, eliminating wasteful and polluting idling in
                        traffic offering significantly quieter buses, ideal for inner-city and suburban usage. Higher
                        operating margins will be achieved through significantly reduced maintenance costs.<br><br>
                        Our hybrid buses make use of the latest technology including battery storage and break energy recuperation.
                        Our improved battery technology, incorporating lithium-iron batteries, provides high power with
                        an improved life expectancy.

                    </p>

                </div>
                <div class="col-md-5 order-md-1 mt-2">
                    <img src="/css/assets/automotive/electric_vechicle.png" class="img-fluid pt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content">
                    <h2>Minibuses</h2>
                    <hr>
                    <p>
                        Our new range of mini-buses offers excellent maneuverability in traffic providing drivers and passengers a
                        superior journey experience, including enhanced climate control and easier boarding and alighting for customers
                        in addition to a number of new enhanced safety features. Our exciting new range offers high quality vehicles,
                        which combine state of the art technology with comfortable and sumptuous interiors at highly affordable prices,
                        with optional leather seats bringing sophistication, comfort and practicality. Equally important the new range
                        is designed to offer maximum luggage storage capacity.<br><br>
                        Built in collaboration with our European partners in our purpose built factory, the new range is packed with ground
                        breaking and innovative technology which can be easily adapted to meet our customers’ specific requirements. This cutting
                        edge technology provides a longer service life and reduced maintenance and repair costs.
                    </p>
                </div>
                <div class="col-md-4 pt-5">
                    <img src="/css/assets/automotive/hybrid_technology1.png" class="img-fluid mt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
@endsection