@extends('layouts.app')
@section('content')
    <section class="sectors-carousel components-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Components</h1>
                    <p>At PPP Group we source components which are designed and rigorously tested to meet exacting
                        standards in line with more sophisticated engine and body designs.
                    </p>
                    <hr>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content">
                    <h2>Overview</h2>
                    <hr>
                    <p>The European automotive industry is among the largest and most innovative in the world,
                        with a strong focus on quality, safety, and sustainability. As this industry transitions towards
                        electric vehicles (EVs), stricter emission standards, and advanced driver-assistance systems
                        (ADAS), the need for expert consultancy in automotive component manufacturing is
                        growing.</p>
                </div>
                <div class="col-md-4 pt-4">
                    <img src="/css/assets/automotive/variable_transmittion5.png" class="img-fluid my-3" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-7 sector-content order-md-12">
                    <p>At PPP European Group, with nearly three decades of experience in this field, our team offer
                        specialised expertise to automotive component manufacturers, assisting them in achieving
                        world-class production capabilities, cost efficiency, and compliance with stringent new
                        regulations. Our services are designed to guide clients through the rapidly changing
                        automotive landscape, focusing on technological innovation, sustainability, and lean
                        manufacturing processes.
                    </p>

                </div>
                <div class="col-md-5 order-md-1 mt-2">
                    <img src="/css/assets/automotive/variable_transmittion5_2.png" class="img-fluid pt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
@endsection