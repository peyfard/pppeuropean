@extends('layouts.app')
@section('content')
    <section class="sectors-carousel property-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Property Development</h1>
                    <hr>
                    <p>At PPP European Group we focus on acquiring, developing, and selling commercial and residential real estate, ensuring each project maximises returns while meeting market needs for eco-friendly and technologically advanced properties.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content">
                    <h2>Property Opportunites</h2>
                    <hr>
                    <p>The real estate development industry, both commercial and residential, continues to
                        experience steady demand driven by urbanisation, population growth, and the need
                        for new infrastructure. With increasing interest in sustainable building practices and
                        smart technologies, there is a significant opportunity for developers to capitalise on
                        modern trends. At PPP European Group we focus on acquiring, developing, and
                        selling commercial and residential real estate, ensuring each project maximises
                        returns while meeting market needs for eco-friendly and technologically advanced
                        properties.</p>
                </div>
                <div class="col-md-4 pt-4">
                    <img src="/css/assets/health-property/commercial-property.png" class="img-fluid pt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-7 sector-content order-md-12">
                    <h2>Commercial &amp; Residential Strategy</h2>
                    <hr>
                    <p>Our business focuses on identifying and acquiring underdeveloped land or properties
                        with potential for redevelopment, followed by constructing or rehabilitating residential
                        and commercial buildings. We cater to a growing demand for eco-friendly, smart,
                        and energy-efficient properties in urban areas. By combining market research,
                        sustainable practices, and high-quality construction, we offer appealing living and
                        working spaces that meet current consumer demands.

                    </p>

                </div>
                <div class="col-md-5 order-md-1 mt-2">
                    <img src="/css/assets/power-petro/energy.png" class="img-fluid my-3" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
@endsection