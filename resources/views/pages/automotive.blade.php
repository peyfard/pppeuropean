@extends('layouts.app')
@section('content')
    <section class="sectors-carousel">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Automotive</h1>
                    <hr>
                    <p>We fully appreciate the importance of driving dynamics, the pleasure
                        and performance expectations of today’s driver, which is always a key consideration.
                        <br>The effortless deployment of power makes a significant difference to the
                        safety and comfort of vehicles improving driver performance.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-grey">
        <div class="container">
            <div class="row">
                <div class="col-12 sector-content">
                    <h2>Internal combustion engines</h2>
                    <hr>
                        As the automotive and industrial sectors face increasing regulatory and
                        environmental pressures, companies that rely on internal combustion engines (ICEs)
                        are looking for ways to innovate, optimise, and transition towards cleaner, more
                        efficient technologies. Despite the rise of electric vehicles (EVs), the ICE market
                        remains crucial, particularly in heavy-duty transportation, marine, and industrial
                        applications.<br><br>
                        At PPP European Group we offer specialised services to manufacturers, fleet
                        operators, and industries that rely on ICEs. These services range from technical
                        optimisation and emissions reduction to compliance with global and regional
                        environmental regulations. Our expertise will position us as a critical partner in
                        helping companies adapt and evolve their ICE-based systems in an era of tightening
                        environmental standards and increasing competition.
                        <br><br>
                        <strong>Vision:</strong> To be the leading consultancy firm in the ICE sector, enabling businesses to
                        transition to cleaner, more efficient internal combustion technologies.<br><br>
                        <strong>Mission:</strong> To provide innovative, practical, and sustainable solutions that help clients
                        optimise their ICE technology and meet global regulatory standards.</p>
                </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-7 sector-content order-md-12">
                    <h2>Electric Vehicles<br>Technology &amp; Components</h2>
                    <hr>
                    <p>
                        The global shift towards electric vehicles (EVs) is accelerating, driven by the demand for
                        sustainable transportation, government incentives, and advancements in battery technology.
                        Manufacturers, suppliers, and infrastructure developers are all under pressure to keep pace
                        with this transition. Our specialised team can assist automotive manufacturers, parts
                        suppliers, infrastructure developers, and investors in optimising their EV-related operations.
                        We offer services ranging from technology selection and component sourcing to
                        manufacturing optimisation, regulatory compliance, and strategic market positioning. Our
                        deep industry expertise will enable us to provide actionable insights and strategies tailored to
                        the fast-growing and competitive EV market.<br><br>
                        Our team specialises in providing end-to-end solutions for businesses operating in or
                        transitioning to the EV sector. We offer services ranging from technology selection and
                        component sourcing to manufacturing optimisation, regulatory compliance, and strategic
                        market positioning.
                        <br><br>
                        <strong>Our Vision:</strong> To be the leading consultancy firm driving innovation and excellence in the
                        global transition to electric vehicle technology.<br><br>
                        <strong>Mission:</strong> To provide expert guidance on EV technologies, helping businesses innovate, scale,
                        and succeed in the new era of electric mobility.
                    </p>
                </div>
                <div class="col-md-5 order-md-1">
                    <img src="/css/assets/automotive/hybrid_technology1.png" class="img-fluid pt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>

@endsection