
@extends('layouts.app')
@section('content')
            <section class="container sector-white aos-item" data-aos="zoom-in">
                <div class="row">
                    <div class="col-md-6 sector-content ethos">
                        <h2>Our Ethos</h2>
                        <hr>
                        <p>
                            At PPP European Group we take our corporate social responsibilities very seriously. We understand that everything we
                            do has an economic, social, and ecological impact. We believe, first and foremost, we are fully accountable and responsible
                            to the people we deal with throughout all aspects of our business dealings.<br><br>
                            We aim to ensure all our stakeholders are treated fairly and with professionalism. Our shareholders, our staff, our customers
                            our suppliers and our fellow citizens of the world matter to us, and this ethos forms the basis of everything we do. We understand
                            that we must always focus on managing and reducing the environmental impact of the businesses within our group.<br><br></p>

                    </div>
                    <div class="col-md-6 my-4">
                        <img src="/css/assets/ethos/our-ethos.png" class="img-fluid my-3 float-md-right" alt="A generic square placeholder image with rounded corners in a figure.">
                    </div>
                </div>

                <div class="row sectors aos-item mt-4" data-aos="zoom-in">
                    <div class="col-sm-3 mt-4">
                        <div class="list-group" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Value to Society</a>
                            <a class="list-group-item list-group-item-action" id="list-components-list" data-toggle="list" href="#list-components" role="tab" aria-controls="components">Employee Welfare</a>
                            <a class="list-group-item list-group-item-action" id="list-petrochemical-list" data-toggle="list" href="#list-petrochemical" role="tab" aria-controls="petrochemical">Supplier Management</a>
                        </div>
                    </div>
                    <div class="col-sm-9 mt-4">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active card sector-information p-3" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                                We are an independent group operating across a variety of business sectors worldwide. Our business ethos is to strive
                                to be fully customer focused by offering value through innovation and service whilst maintaining our commitment to environment
                                integrity. Our business partners can be confident in the knowledge that we always strive to work openly and fairly. We understand
                                the importance of working collaboratively. PPP Group places great importance on effective and open communication.
                            </div>
                            <div class="tab-pane fade card sector-information p-3" id="list-components" role="tabpanel" aria-labelledby="list-components-list">
                                We believe our business is built from the talent of our staff and that is why we invest in leadership training programmes
                                and have created an inclusive and dynamic workplace. <br><br>
                            </div>
                            <div class="tab-pane fade card sector-information p-3" id="list-petrochemical" role="tabpanel" aria-labelledby="list-petrochemical-list">
                                We work hard to avoid, mitigate and managing environmental and social impacts across our projects. As
                                an organisation, honesty and integrity is core to the culture of PPP and we ensure that the businesses we work with
                                have the same ethos and transparency in their operations.
                            </div>
                        </div>
                    </div>
                </div>

            </section>
@endsection