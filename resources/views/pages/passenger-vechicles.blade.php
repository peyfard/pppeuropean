@extends('layouts.app')
@section('content')
    <section class="sectors-carousel passenger-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <h1>Passenger Vechicles</h1>
                    <p>We work with our business partners to develop innovative and
                        efficient powertrain solutions to reduce fuel consumption and
                        carbon emissions. We fully appreciate the importance of driving dynamics,
                        the pleasure and performance expectations of today’s driver, which is always a key consideration.
                    </p>
                    <hr>
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content">
                    <h2>Powertrain Systems</h2>
                    <hr>
                    <p>The effortless deployment of power makes a significant difference to the safety and comfort of vehicles improving driver performance.
                        Internal combustion engines are still the preferred choice for many of our global customers, especially where engines must deliver
                        speed and power. We work closely with our business partners to ensure power and increased fuel efficiency for both petrol and
                        diesel engines is achieved through innovation and technology.<br><br>
                        At PPP Group we offer a wide range of Continuously Variable Transmissions (CVT), for conventional, hybrid, start/stop systems
                        and mild hybrids. Hybrid technology offers an increasingly sophisticated range of options, which are now available through the
                        varying degrees of electrification within hybrid powertrains. The electric motor provides assistance to the internal combustion engine
                        through delivering high torque driving, providing acceleration from lower speeds and inclines, making it possible to downsize engines
                        whilst maintaining increasingly higher levels of performance. Through our innovation and technology we are able to help our customers
                        achieve more economical vehicles without compromising on comfort or performance. Electric vehicles are far more than just an
                        environmental choice in today’s market.<br><br>
                        Through improved electric charger converter technology, such as the integrated charger-inverter,
                        affordable and efficient electric cars are now being produced on a large scale. Our option of switch reluctance motor offered through our
                        chosen partner forms the heart of our electric powertrains, offering further flexibility of design options. Our technological innovations enable
                        our customers to reduce the cost of electrical components, achieving highly desirable and affordable electric cars..</p>
                </div>
                <div class="col-md-4 pt-4">
                    <img src="/css/assets/automotive/continuously_variable_transmittion4.png" class="img-fluid" alt="A generic square placeholder image with rounded corners in a figure.">
                    <img src="/css/assets/automotive/continuously_variable_transmittion1.png" class="img-fluid" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-7 sector-content order-md-12">
                    <h2>Commercial Vechicles</h2>
                    <hr>
                    <p>Our involvement with commercial vehicles dates back to the 1950’s, where one of our company’s founders started his first
                        coach company. His passion and experience has been the driving force behind our commercial vehicle manufacturing business.<br><br>
                        Our industry insight and understanding of how the transport industry operates has enabled us to design vehicles which offer
                        minimum cost of ownership, low operating costs and reduced uptime enabling our customers to achieve a better rate of return
                        on their investment. As part of our corporate social responsibility strategy, in addition to reducing carbon footprints by improving
                        fuel efficiency, we aim to incorporate where possible fully recyclable materials in all our vehicles.</p>

                </div>
                <div class="col-md-5 order-md-1 mt-2">
                    <img src="/css/assets/automotive/commercial-vechicles.png" class="img-fluid pt-4" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
    <section class="sector-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 sector-content">
                    <h2>Coaches</h2>
                    <hr>
                    <p>To be launched in next year, APF Coaches, a subsidiary of PPP European Group in the Middle East, will be showcasing a new range
                        of coaches. Our exciting new range will offer high quality vehicles, which combine state of the art technology with comfortable
                        and sumptuous interiors at highly affordable prices.<br><br>
                        Built in collaboration with our European partners, in our purpose built factory,
                        the new range is packed with ground breaking and innovative technology. Our cutting edge technology provides long service life and
                        reduced maintenance and repair costs. Our new range of coaches, are designed for long-haul journeys, offering great comfort and
                        safety whilst achieving low fuel consumption and maximum environmental protection.</p>
                </div>
                <div class="col-md-4 pt-5">
                    <img src="/css/assets/automotive/coach.png" class="img-fluid mt-3" alt="A generic square placeholder image with rounded corners in a figure.">
                </div>
            </div>
        </div>
    </section>
@endsection