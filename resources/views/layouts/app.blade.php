<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="8YwgWFRumrysaTXmJoNIqEW_04GWsaHjt-URWr3d9Ps" />


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PPP European Group - @isset($title)
            {{ $title }}
        @endisset

        @empty($title)

        @endempty</title>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:100" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-parallax/1.1.3/jquery-parallax-min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>



</head>
<body>
    <main>
        @include('inc.navbar')
        @if (Cookie::get('ftcookie') !== null)
            <!--<p>{{Cookie::get('ftcookie')}}</p>-->
            @else
                            <div class="alert alert-primary fade show m-0" role="alert">
                                This site uses cookies to provide you with a great user experience. Please either
                                <a style="font-weight: bold;" href="/cookie"> Grant Permission</a> or read our <a style="font-weight: bold;" href="#"> Privacy Policy</a>
                            </div>
            @endif
        @include('inc.messages')
        @yield('content')

        @include('inc.footer')
    </main>


    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init({
            easing: 'ease-in-out-sine'
        });
    </script>


    @stack('scripts')

</body>
</html>
