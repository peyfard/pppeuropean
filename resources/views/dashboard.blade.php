@extends('layouts.app')

@section('content')
<div class="container my-4">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row justify-content-end">
                        <div class="col-md-12 animated fadeIn fast">
                            @if(Auth::user()->access_level == 1 or Auth::user()->access_level == 2)
                                <a href="/categories/create" class="btn btn-success float-right  mr-2">Create Category</a>
                            @endif
                            <a href="/posts/create" class="btn btn-primary float-right mr-2">Create Post</a>
                        </div>
                   </div>
                        <hr class=" py-3">

                        @if(count($posts) > 0)
                            <table class="table table-borderless">
                                <thead class="thead-light">
                                    <tr>
                                        <th style="width: 60%">My Posts</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td class="pt-4"><a href="/posts/{{$post->slug}}">{{$post->title}}</a></td>
                                            <td><a href="/posts/{{$post->id}}/edit" class="btn btn-outline-primary ml-2">Edit</a> </td>
                                            <td>
                                                {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-left ml-2'])!!}
                                                    {{Form::hidden('_method', 'DELETE')}}
                                                    {{Form::submit(' Delete ', ['class' => 'btn btn-outline-danger'])}}
                                                {!!Form::close()!!}
                                            </td>
                                        </tr>
                                    @endforeach
                            </table>
                        @else
                            <p>You have no posts.</p>
                        @endif

                        <br>
                        <hr class="py-3">
                        @if(Auth::user()->access_level == 1 or Auth::user()->access_level == 2)
                            @if(count($topics) > 0)
                                <table class="table table-borderless">
                                    <thead class="thead-light">
                                    <tr>
                                        <th style="width: 60%"> Post Categories</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    @foreach($topics as $topic)
                                        <tr>
                                            <td class="pt-4"><a href="/post/categories/{{$topic->slug}}">{{$topic->title}}</a></td>
                                            <td><a href="/categories/{{$topic->id}}/edit" class="btn btn-outline-primary ml-2">Edit</a> </td>
                                            <td>
                                                {!!Form::open(['action' => ['TopicsController@destroy', $topic->id], 'method' => 'POST', 'class' => 'float-left ml-2'])!!}
                                                {{Form::hidden('_method', 'DELETE')}}
                                                {{Form::submit(' Delete ', ['class' => 'btn btn-outline-danger'])}}
                                                {!!Form::close()!!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            @else
                                <p>There are no categories.</p>
                            @endif
                        @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
