@extends('layouts.app')

@section('content')

    <div class="container my-3">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">My Profile</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <hr>
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                        @if(Auth::user()->access_level == 1)
                                            <p>Admin</p>
                                        @endif
                                        @if(Auth::user()->access_level == 2)
                                            <p>Content Manager</p>
                                        @endif
                                        @if(Auth::user()->access_level == 3)
                                            <p>Content Writer</p>
                                        @endif

                                </td>
                                <td><a href="/users/{{$user->id}}/edit" class="btn btn-outline-secondary">Edit Profile</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
