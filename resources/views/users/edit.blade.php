@extends('layouts.app')

@section('content')

    <div class="container my-3">
        <div class="row" style="margin-bottom: 50px;">
            <div class="col-md-12">

                @if(Auth::user()->access_level == 1)
                    <a href="/users/" class="btn btn-outline-secondary float-lg-right" style="margin-bottom: 20px;">Go Back</a>
                @else
                    <a href="/users/{{$user->id}}" class="btn btn-outline-secondary float-lg-right" style="margin-bottom: 20px;">Go Back</a>
                @endif

                <h3>Edit Profile</h3>
                <br><hr>


                {!! Form::open(['action' => ['UserController@update', $user->id], 'method' => 'POST']) !!}

                    <div class="form-group">
                    {{Form::label('name', 'Name')}}
                    {{Form::text('name', $user->name, ['class' => 'form-control'])}}
                    </div>
                    @if(Auth::user()->access_level == 1)
                        <div class="form-group">
                        {{Form::label('access_level', 'Role')}}<br>
                        {{Form::select('access_level', [3 => 'Content Writer', 2 => 'Content Manager', 1 => 'Site Admin'], $user->access_level)}}
                        </div>
                    @endif
                    @if(Auth::user()->access_level !== 1)
                        {{ Form::hidden('access_level', $user->access_level) }}
                    @endif


                    <div class="form-group">
                    {{Form::label('email', 'Email')}}
                    {{Form::text('email', $user->email, ['class' => 'form-control'])}}

                    </div>


                {{Form::hidden('_method', 'PUT')}}

                {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection