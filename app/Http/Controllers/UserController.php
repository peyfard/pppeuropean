<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(auth()->user()->access_level !== 1){
            return redirect('/')->with('error', 'Unauthorised page');
        }
        return view('users.index');
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $users = DB::table('users')->where('name', 'LIKE', '%' . $request->search . "%")->get();
            if ($users) {
                foreach ($users as $user) {
                    $output .= '<tr>' .
                        '<td>' . $user->name . '</td>' .
                        '<td>' . $user->email . '</td>' .
                        '<td>' . $user->access_level . '</td>' .
                        '<td>' . $user->updated_at . '</td>' .
                        '<td><a href="/users/' . $user->id . '/edit" class="btn btn-outline-primary ml-2">Edit</a></td>' .
                        '</tr>';
                }
                return Response($output);
            }
        }
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $current_user_id = $user->id;

        // Allow admin access to any show record
        if(auth()->user()->access_level == 1){
            return view('users.show')->with('user', $user);
        }

        // Redirect a user if they are trying to view someone elses show record
        if(auth()->user()->id !== $current_user_id){
            return redirect('/users')->with('error', 'Unauthorised page');
        }

        return view('users.show')->with('user', $user);
    }

    public function edit($id)
    {
        //Get User Id
        $user = User::findOrFail($id);
        $current_user_id = $user->id;

        //Allow Access if Admin
        if(auth()->user()->access_level == 1){
            return view('users.edit')->with('user', $user);
        }

        //Allow Access if User is Editing Their Own Record
        if(auth()->user()->id !== $current_user_id){
            return redirect('/users')->with('error', 'Unauthorised page');
        }

        return view('users.edit')->with('user', $user);

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required'
        ]);

        $user = User::find($id);
        $user->updated_at = now();
        $user->name = $request->input('name');
        $user->access_level = $request->input('access_level');
        $user->email = $request->input('email');
        $user->save();

        // Redirect user according to their access level
        if(auth()->user()->access_level !== 1) {
        return redirect('/dashboard')->with('success', 'User Updated');
        }
        if(auth()->user()->access_level == 1) {
            return redirect('/users')->with('success', 'User Updated');
        }

    }

    public function destroy($id)
    {
        $user = User::find($id);

        // Check is user is admin and redirects if not
        if(auth()->user()->access_level !== 1){
            return redirect('/posts')->with('error', 'Unauthorised page');
        }

        // Delete user and redirect to users index view
        $user->delete();
        return redirect('/users/')->with('success', 'User Removed');
    }

}