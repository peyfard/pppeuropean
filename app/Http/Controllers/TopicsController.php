<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Topic;

class TopicsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $topics =  Topic::has('posts')->orderBy('created_at', 'asc')->paginate(6);
        $title = 'News and Articles Categories';
        return view('topics.index', compact('title', 'topics'));

    }

    public function create()
    {
        // Check for admin or content manager
        if(auth()->user()->access_level > 2){
            return redirect('/post/categories')->with('error', 'Unauthorised Page');
        }

        return view('topics.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $topic = new Topic;
        $topic->slug = '';
        $topic->title = $request->input('title');
        $topic->save();
        return redirect('/post/categories')->with('success', 'Category Created');
    }

    public function show($slug)
    {
        $topic = Topic::where('slug', $slug)->firstOrFail();
        $topicposts = $topic->posts()->paginate(5);
        $topictitle = $topic->title;

        $title = $topictitle;

        return view('topics.show')->with('posts', $topicposts)->with(compact('topictitle', 'title'));
    }

    public function edit($id)
    {
        $topic = Topic::find($id);

        // Check for Admin or Content Manager
        if(auth()->user()->access_level > 2){
            return redirect('/post/categories')->with('error', 'Unauthorised Page');
        }

        return view('topics.edit')->with('topic', $topic);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $topic = Topic::find($id);
        $topic->title = $request->input('title');
        $topic->save();

        return redirect('/post/categories')->with('success', 'Category Updated');

    }

    public function destroy($id)
    {
        $topic = Topic::find($id);
        $topic->delete();
        return redirect('/post/categories')->with('error', 'Category Deleted');
    }
}
