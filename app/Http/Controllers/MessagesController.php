<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessagesController extends Controller
{
    public function create()
    {
        $title = 'Contact Us';
        return view('pages.contact', compact('title'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $message = new Message;
        $message->name = $request->input('name');
        $message->email = $request->input('email');
        $message->company = $request->input('company');
        $message->reason = $request->input('reason');
        $message->body = $request->input('body');
        $message->save();
        return redirect('/post/categories')->with('success', 'Message Created');
    }

    public function index()
    {
        $messages =  Message::orderBy('created_at', 'asc')->paginate(6);
        return view('messages')->with('messages', $messages);
    }
}
