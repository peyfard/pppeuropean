<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use App\Post;
use App\Topic;
//use DB;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $posts =  Post::all();
        // $post = Post::where('title', 'Post Two')->get();
        // $posts =  Post::orderBy('title', 'desc')->take(1)->get();
        // $posts =  Post::orderBy('title', 'asc')->get();

        $posts =  Post::orderBy('created_at', 'asc')->paginate(6);
        $title = 'News and Articles';
        return view('posts.index', compact('title', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $topics = Topic::all();
        $topics = $topics->map(function ($topic) {
            return collect($topic->toArray())
                ->only(['id', 'slug'])
                ->all();
        });

        $topics = array_pluck($topics, 'slug', 'id');
        //return view('posts.create')->with('topics', $topics);

        $title = 'Create Post';
        return view('posts.create', compact('title', 'topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);


        // Handle File Upload
        if($request->hasFile('cover_image')){

            // Get filename with the extention
            $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();

            // Get just filename
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();

            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            // Upload the image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        } else {
            $fileNameToStore = 'noimage.jpg';
        }


        // Create Post
        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->topic_id = $request->input('topic');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();

        return redirect('/market-insights')->with('success', 'Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  varchar  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();

        $title = $post->title;
        return view('posts.show', compact('title', 'post'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topics = Topic::all();
        $topics = $topics->map(function ($topic) {
            return collect($topic->toArray())
                ->only(['id', 'slug'])
                ->all();
        });

        $topics = array_pluck($topics, 'slug', 'id');


        $post = Post::find($id);

        // Check for correct user
        if(auth()->user()->id !== $post->user_id){
            return redirect('/posts')->with('error', 'Unauthorised Page');
        }

        return view('posts.edit')->with('post', $post)->with('topics', $topics);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        // Handle File Upload
        if($request->hasFile('cover_image')){

            // Get The File Name With Extention
            $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();

            // Get Just File Name
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get Just Ext
            $extention = $request->file('cover_image')->getClientOriginalExtension();

            // File Name To Store
            $fileNameToStore= $filename.'_'.time().'.'.$extention;

            // Upload The Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        }

        // Create Post //
        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->topic_id = $request->input('topic');
        $post->body = $request->input('body');
        $post->created_at = $request->input('created_at');
        if($request->hasFile('cover_image')){
            $post->cover_image = $fileNameToStore;
        }
        $post->save();

        return redirect('/market-insights')->with('success', 'Post Updated');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        // Check for correct user
        if(auth()->user()->id !== $post->user_id){
            return redirect('/posts')->with('error', 'Unauthorised Page');
        }

        if($post->cover_image != 'noimage.jpg'){
            // Delete Image
            Storage::delete('public/cover_images/'.$post->cover_image);
        }

        $post->delete();

        return redirect('/posts')->with('error', 'Post Deleted');
    }
}
