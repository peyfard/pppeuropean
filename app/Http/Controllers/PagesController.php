<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\URL;

class PagesController extends Controller
{
    public function index(){
        $title = 'Home';
        return view('pages.index', compact('title'));
    }

    public function ethos(){
        $title = 'Our Ethos';
        return view('pages.ethos')->with('title', $title)->withCookie(cookie()->queue('confirmed', 1, 45000));
    }

    public function automotive(){
        $title = 'Automotive';
        return view('pages.automotive', compact('title'));
    }

    public function passengerVechicles(){
        $title = 'Passenger Vechicles';
        return view('pages.passenger-vechicles', compact('title'));
    }

    public function buses(){
        $title = 'Buses';
        return view('pages.buses', compact('title'));
    }

    public function components(){
        $title = 'Components';
        return view('pages.components', compact('title'));
    }

    public function petrochemical(){
        $title = 'Petrochemical';
        return view('pages.petrochemical', compact('title'));
    }

    public function powerGeneration(){
        $title = 'Power Generation';
        return view('pages.power-generation', compact('title'));
    }

    public function propertyDevelopment(){
        $title = 'Property Development';
        return view('pages.property-development', compact('title'));
    }

    public function health(){
        $title = 'Health & Supplements';
        return view('pages.health', compact('title'));
    }

    public function skincare(){
        $title = 'Skincare';
        return view('pages.skincare', compact('title'));
    }

    public function cookiePolicy(){
        $title = 'Cookie Policy';
        return view('pages.cookies', compact('title'));
    }

    public function termsAndConditions(){
        $title = 'Terms and Conditions';
        return view('pages.terms', compact('title'));
    }

    public function cookie(){
        cookie()->queue('ftcookie', 'football target', 4500);
        return redirect(URL::previous());
    }

    public function sitemap()
    {
        return response()->view('pages.sitemap')->header('Content-Type', 'text/xml');
    }

}