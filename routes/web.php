<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/our-ethos', 'PagesController@ethos');

Route::get('/sitemap.xml', 'PagesController@sitemap');

Route::get('/cookie', 'PagesController@cookie');

Route::get('/sectors/automotive', 'PagesController@automotive');
//Route::get('/sectors/passenger-vechicles', 'PagesController@passengerVechicles');
//Route::get('/sectors/buses', 'PagesController@buses');
Route::get('/sectors/components', 'PagesController@components');
Route::get('/sectors/petrochemical', 'PagesController@petrochemical');
Route::get('/sectors/power-generation', 'PagesController@powerGeneration');
Route::get('/sectors/property-development', 'PagesController@propertyDevelopment');
Route::get('/sectors/health-and-supplements', 'PagesController@health');
Route::get('/sectors/skincare', 'PagesController@skincare');

Route::get('/cookie-policy', 'PagesController@cookiePolicy');
Route::get('/terms-and-conditions', 'PagesController@termsAndConditions');

Route::get('/contact-us', 'MessagesController@create');
Route::post('/contact-us', 'MessagesController@store');
Route::get('/dashboard/messages', 'MessagesController@index');

//Route::resource('posts', 'PostsController');

Route::get('/market-insights/', 'PostsController@index');
Route::get('/posts/create', 'PostsController@create');
Route::post('/posts', 'PostsController@store');
Route::get('/market-insights/{slug}', 'PostsController@show');
Route::get('/posts/{id}/edit', 'PostsController@edit');
Route::patch('/posts/{id}', 'PostsController@update');
Route::delete('/posts/{id}', 'PostsController@destroy');

Auth::routes();
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});




Route::get('/post/categories/', 'TopicsController@index');
Route::get('/categories/create', 'TopicsController@create');
Route::post('/categories', 'TopicsController@store');
Route::get('/post/categories/{slug}', 'TopicsController@show');
Route::get('/categories/{id}/edit', 'TopicsController@edit');
Route::patch('/categories/{id}', 'TopicsController@update');
Route::delete('/categories/{id}', 'TopicsController@destroy');

Route::get('/users/', 'UserController@index');
Route::post('/users', 'UserController@store');
Route::get('/users/{id}', 'UserController@show');
Route::get('/users/{id}/edit', 'UserController@edit');
Route::put('/users/{id}', 'UserController@update');
Route::delete('/users/{id}', 'UserController@destroy');


Route::get('/search','UserController@search');

Route::get('/dashboard', 'DashboardController@index');
